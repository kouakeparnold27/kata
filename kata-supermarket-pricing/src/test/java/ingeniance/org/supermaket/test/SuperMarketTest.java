package ingeniance.org.supermaket.test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import ingeniance.org.kata.supermarket.controller.ProductController;
import ingeniance.org.kata.supermarket.data.schema.Category;
import ingeniance.org.kata.supermarket.data.schema.Payment;
import ingeniance.org.kata.supermarket.data.schema.PaymentDetail;
import ingeniance.org.kata.supermarket.data.schema.Product;
import ingeniance.org.kata.supermarket.data.schema.Rate;
import ingeniance.org.kata.supermarket.enums.EnumStatus;
import ingeniance.org.kata.supermarket.service.CategoryService;
import ingeniance.org.kata.supermarket.service.PaymentDetailService;
import ingeniance.org.kata.supermarket.service.PaymentService;
import ingeniance.org.kata.supermarket.service.ProductService;
import ingeniance.org.kata.supermarket.service.RateService;

@SpringBootTest
@AutoConfigureMockMvc
public class SuperMarketTest {

//	public static final Product product = Product.builder().productId(1l).code("01").proName("HUILE")
//			.description("Huile Raffinee").build();
//
//	public static final Category category = Category.builder().categoryId(1l).product(product)
//			.conditionnement("Bouteille").description("Huile Mayor Bon pour la sante.").label("KSA").unity("L")
//			.unitValue(1).quantity(1).build();
//
//	public static final Rate rate = Rate.builder().rateId(1L).category(CategoryControllerTU.category).quantity(1)
//			.quantityAmount(50D).build();
//
//	private static final Set<PaymentDetail> paymentDetailSet = new HashSet<>();
//
//	public static final Payment payment = Payment.builder().paymentId(1L).paymentDetailSet(paymentDetailSet).build();

	@Autowired
	MockMvc mvc;

	@MockBean
	ProductService productService;

	@MockBean
	CategoryService categoryService;

	@MockBean
	RateService rateService;

	@MockBean
	PaymentService paymentService;

	@MockBean
	PaymentDetailService paymentDetailService;

	@Autowired
	ProductController productController;

	@Test
	public void testSimplePrice() throws Exception {

//		MockHttpServletRequest request = new MockHttpServletRequest();
//		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
//
//		when(productService.save(any())).thenReturn(ProductControllerTU.product);
//		productService.save(ProductControllerTU.product);
//		
//		when(categoryService.save(any())).thenReturn(CategoryControllerTU.category);
//		categoryService.save(CategoryControllerTU.category);
//
//		when(rateService.save(any())).thenReturn(RateControllerTU.rate);
//		rateService.save(RateControllerTU.rate);

//		rateService.getAllRates();
//		assertThat(productService.getReferenceId(1l)).isNull();

		Product product = Product.builder().productId(1l).code("01").proName("beans").description("can of beans")
				.build();

		Category category = Category.builder().categoryId(1l).product(product).conditionnement("can")
				.description("can of beans").label("beans").unity(EnumStatus.Kilogramm.toString()).unitValue(1).quantity(1).build();

		Rate rate = Rate.builder().rateId(1L).category(category).quantity(1).quantityAmount(0.65D).build();

		List<Rate> rateList = Arrays.asList(rate);

		when(rateService.getRatesOfCategory(any())).thenReturn(rateList);
		mvc.perform(get("/kata/supermarket/price/{category}", category.getCategoryId()))
				.andExpect(status().isOk()).andDo(print());
	}

	@Test
	public void testGetTreeForOneDollar() throws Exception {

		Product product = Product.builder().productId(1l).code("01").proName("beans").description("can of beans")
				.build();

		Category category = Category.builder().categoryId(1l).product(product).conditionnement("can")
				.description("can of beans").label("beans").unity(EnumStatus.Kilogramm.toString()).unitValue(1).quantity(1).build();

		Rate rate = Rate.builder().rateId(1L).category(category).quantity(3).quantityAmount(1D).build();

		PaymentDetail paymentDetail = PaymentDetail.builder().paymentDetailId(1L).quantity(4).rate(rate).build();
		Set<PaymentDetail> paymentDetailSet = new HashSet<>();
		paymentDetailSet.add(paymentDetail);
		Payment payment = Payment.builder().paymentId(1L).paymentDetailSet(paymentDetailSet).build();


		PaymentDetail paymentDetail2 = PaymentDetail.builder().paymentDetailId(2L).quantity(5).rate(rate).build();
		Set<PaymentDetail> paymentDetail2Set = new HashSet<>();
		paymentDetail2Set.add(paymentDetail2);
		Payment payment2 = Payment.builder().paymentId(1L).paymentDetailSet(paymentDetail2Set).build();

		
		when(rateService.getReferenceById(any())).thenReturn(rate);

		when(paymentService.getReferenceById(payment.getPaymentId())).thenReturn(payment);
		mvc.perform(get("/kata/supermarket/price").param("payment", payment.getPaymentId().toString()))
				.andExpect(status().isOk()).andDo(print());
		
		

		when(paymentService.getReferenceById(payment2.getPaymentId())).thenReturn(payment2);
		mvc.perform(get("/kata/supermarket/price").param("payment", payment2.getPaymentId().toString()))
				.andExpect(status().isOk()).andDo(print());
		
	}
	
	
	/**
	 * Le principe se fait au niveau de l'unite.
	 * L'unite par defaut est l'once,
	 * L'unite se choisi sur la categorie du produit, ainsi que sur le paiement du produitl
	 * S'ils sont differents, la conversion est faire selon le tableau de conversion initialise.
	 * 
	 * */
	@Test
	public void testGetPriceWithConvertion() throws Exception {

		Product product = Product.builder().productId(1l).code("01").proName("beans").description("can of beans")
				.build();

		Category category = Category.builder().categoryId(1l).product(product).conditionnement("can")
				.description("can of beans").label("beans").unity(EnumStatus.Pound.toString()).unitValue(1.99D).quantity(1).build();

		Rate rate = Rate.builder().rateId(1L).category(category).quantity(1).quantityAmount(1.99D).build();

		PaymentDetail paymentDetail = PaymentDetail.builder().paymentDetailId(1L).unity(EnumStatus.Ounce.toString()).quantity(4).rate(rate).build();
		Set<PaymentDetail> paymentDetailSet = new HashSet<>();
		paymentDetailSet.add(paymentDetail);
		Payment payment = Payment.builder().paymentId(1L).paymentDetailSet(paymentDetailSet).build();

		when(rateService.getReferenceById(any())).thenReturn(rate);

		when(paymentService.getReferenceById(payment.getPaymentId())).thenReturn(payment);
		mvc.perform(get("/kata/supermarket/price").param("payment", payment.getPaymentId().toString()))
				.andExpect(status().isOk()).andDo(print());
		
	}

/**
 * Le produit est categorise : l'unite + prix unitaire. Ex : quantity=2, unitValue=10$
 * La vente promotionnelle est faite quand la grille de prix change sur Rate : quantity=3,  quantityAmount=10$
 * Le principe devient simple : Pour deux acheter, un offert, il s'agit simplement de vendre 3 au prix de 2
 * Dans ce cas, le 3e produit aura une valeur.
 * */
	@Test
	public void testByeTwoGetOneFree() throws Exception {

		Product product = Product.builder().productId(1l).code("01").proName("beans").description("can of beans")
				.build();

		Category category = Category.builder().categoryId(1l).product(product).conditionnement("can")
				.description("can of beans").label("beans").unity(EnumStatus.Pound.toString()).unitValue(10D).quantity(2).build();

		Rate rate = Rate.builder().rateId(1L).category(category).quantity(3).quantityAmount(10D).build();

		PaymentDetail paymentDetail = PaymentDetail.builder().paymentDetailId(1L).unity(null).quantity(3).rate(rate).build();
		Set<PaymentDetail> paymentDetailSet = new HashSet<>();
		paymentDetailSet.add(paymentDetail);
		Payment payment = Payment.builder().paymentId(1L).paymentDetailSet(paymentDetailSet).build();

		when(rateService.getReferenceById(any())).thenReturn(rate);

		when(paymentService.getReferenceById(payment.getPaymentId())).thenReturn(payment);
		mvc.perform(get("/kata/supermarket/price").param("payment", payment.getPaymentId().toString()))
				.andExpect(status().isOk()).andDo(print());
		
	}
}
