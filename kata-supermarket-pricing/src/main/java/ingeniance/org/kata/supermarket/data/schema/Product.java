package ingeniance.org.kata.supermarket.data.schema;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "product", uniqueConstraints = { @UniqueConstraint(columnNames = { "code", "state_db_uniq" }) })
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property = "@product", scope = Product.class, resolver = ObjectIdResolver.class)
public class Product implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "product_id", nullable = false)
	private Long productId;

	@Column(nullable = false, length = 100)
	private String code;

	@Column(name = "pro_name", nullable = false, length = 100)
	private String proName;

	@Column(nullable = false, length = 255)
	private String description;

	@Column(name = "state_db", nullable = false, length = 10)
	private String stateDb;

	@Column(name = "state_db_uniq", nullable = false)
	private long stateDbUniq;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "product", fetch = FetchType.LAZY)
	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	@JsonIgnore
	private Set<Category> categorySet;
}
