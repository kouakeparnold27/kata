package ingeniance.org.kata.supermarket.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ingeniance.org.kata.supermarket.data.schema.Product;
import ingeniance.org.kata.supermarket.enums.EnumStatus;

public interface ProductRepo extends JpaRepository<Product, Long> {

	@Query("FROM Product WHERE stateDb<>:stateDb")
	List<Product> findAll(@Param("stateDb") String stateDb);

	@Query("FROM Product WHERE productId=:productId AND stateDb<>:stateDb")
	Optional<Product> findById(@Param("productId") Long productId, @Param("stateDb") String stateDb);

	@Query("FROM Product WHERE productId=:productId AND stateDb<>:stateDb")
	Product getReferenceById(@Param("productId") Long productId, @Param("stateDb") String stateDb);

	@Query("FROM Product WHERE code=:code AND stateDb<>:stateDb")
	Product getProductByCode(@Param("code") String code, @Param("stateDb") String stateDb);

	@Override
	default List<Product> findAll() {
		return findAll(EnumStatus.DELETE.toString());
	}

	@Override
	default Product getReferenceById(Long id) {
		return getReferenceById(id, EnumStatus.DELETE.toString());
	}

	default Product getProductByCode(String code) {
		return getProductByCode(code, EnumStatus.DELETE.toString());
	}

}
