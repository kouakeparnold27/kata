package ingeniance.org.kata.supermarket.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ingeniance.org.kata.supermarket.business.SuperMarketBusiness;
import ingeniance.org.kata.supermarket.data.schema.Payment;
import ingeniance.org.kata.supermarket.data.schema.Rate;
import ingeniance.org.kata.supermarket.service.PaymentService;
import ingeniance.org.kata.supermarket.service.RateService;
import ingeniance.org.kata.supermarket.service.SuperMarketService;

@Service
public class SuperMarketServiceImp implements SuperMarketService {

//	@Autowired
//	private ProductService productService;
//
//	@Autowired
//	private CategoryService categoryService;

	@Autowired
	private RateService rateService;

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private SuperMarketBusiness spmBusiness;

	@Override
	public String getSimplePrice(Long categoryId) throws Exception {
		String prices = "";

		List<Rate> rates = rateService.getRatesOfCategory(categoryId);
		if (rates == null || rates.size() == 0) {
			prices = "No product found";
		} else
			for (Rate r : rates) {
				prices += String.join(prices,
						r.getQuantity()
								+ (r.getCategory().getUnity() == null
										? " " + r.getCategory().getProduct().getDescription()
										: r.getCategory().getUnity() + " of "
												+ r.getCategory().getProduct().getProName())
								+ " coats " + r.getQuantityAmount())
						+ "$";
			}
		return prices;
	}

	@Override
	public String getTreeForOneDollar(Long paymemtId) throws Exception {
		String prices = "";

		Payment payment = paymentService.getReferenceById(paymemtId);

		Double totalAmount = spmBusiness.calculPrincing(payment.getPaymentDetailSet());

		if (payment != null)
			prices = "Total amount : " + totalAmount.intValue() + "$";
		return prices;
	}
}
