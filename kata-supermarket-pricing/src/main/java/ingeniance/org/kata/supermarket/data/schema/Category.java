package ingeniance.org.kata.supermarket.data.schema;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "category", uniqueConstraints = { @UniqueConstraint(columnNames = { "label", "state_db_uniq" }),
		@UniqueConstraint(columnNames = { "product_id", "conditionnement", "unity", "unit_value", "state_db_uniq" }) })
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property = "@category", scope = Category.class, resolver = ObjectIdResolver.class)
public class Category implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "category_id", nullable = false)
	private Long categoryId;

	@Column(nullable = false, length = 100)
	private String label;

	@Column(nullable = false, length = 200)
	private String description;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "product_id", nullable = false)
	private Product product;

	@Column(nullable = false, length = 30)
	private String conditionnement;

	@Column(nullable = false, length = 20)
	private String unity;

	@Column(name = "unit_value", nullable = false, precision = 15, scale = 3)
	private double unitValue;

	@Column(nullable = false)
	private int quantity;

	@Column(length = 20)
	private String status;

	@Column(name = "dt_created", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtCreated;

	@Column(name = "dt_modified", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtModified;

	@Column(name = "state_db", nullable = false, length = 10)
	private String stateDb;

	@Column(name = "state_db_uniq", nullable = false)
	private long stateDbUniq;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "category", fetch = FetchType.LAZY)
	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	@JsonIgnore
	private Set<Rate> rateSet;

}