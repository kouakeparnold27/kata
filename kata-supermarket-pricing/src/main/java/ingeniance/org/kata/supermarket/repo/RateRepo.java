package ingeniance.org.kata.supermarket.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ingeniance.org.kata.supermarket.data.schema.Rate;
import ingeniance.org.kata.supermarket.enums.EnumStatus;

public interface RateRepo extends JpaRepository<Rate, Long> {

	@Query("FROM Rate WHERE rateId=:rateId AND stateDb<>:stateDb")
	Rate getReferenceById(@Param("rateId") Long rateId, @Param("stateDb") String stateDb);

	@Query("FROM Rate WHERE category.categoryId=:categoryId AND stateDb<>:stateDb")
	List<Rate> getAllRatesOfCategory(@Param("categoryId") Long categoryId, @Param("stateDb") String stateDb);

	@Query("FROM Rate WHERE state=:state AND stateDb<>:stateDb")
	List<Rate> getRatesByState(@Param("state") String state, @Param("stateDb") String stateDb);

	@Query("FROM Rate WHERE category.categoryId=:categoryId ORDER BY dtModified DESC")
	List<Rate> getHistoryRatesByCategory(@Param("categoryId") Long categoryId);
	
	@Override
	default Rate getReferenceById(Long id) {
		return getReferenceById(id, EnumStatus.DELETE.toString());
	}

	default List<Rate> getAllRatesOfCategory(Long categoryId) {
		return getAllRatesOfCategory(categoryId, EnumStatus.DELETE.toString());
	}

	default List<Rate> getRatesByState(String state) {
		return getRatesByState(state, EnumStatus.DELETE.toString());
	}

}
