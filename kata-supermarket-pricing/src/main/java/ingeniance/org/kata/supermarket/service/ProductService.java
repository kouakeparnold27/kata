package ingeniance.org.kata.supermarket.service;

import java.util.List;

import ingeniance.org.kata.supermarket.data.schema.Product;

public interface ProductService {
	public Product save(Product product) throws Exception;

	public void delete(Product product) throws Exception;
	
	public List<Product> getAllProducts() throws Exception;

	public Product getProductByCode(String code) throws Exception;

	Product getReferenceId(Long productId) throws Exception;
}
