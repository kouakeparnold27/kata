package ingeniance.org.kata.supermarket.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ingeniance.org.kata.supermarket.data.schema.Category;
import ingeniance.org.kata.supermarket.enums.EnumStatus;

public interface CategoryRepo extends JpaRepository<Category, Long> {

	@Query("FROM Category WHERE categoryId=:categoryId AND stateDb<>:stateDb")
	Category getReferenceById(@Param("categoryId") Long productId, @Param("stateDb") String stateDb);

	@Query("FROM Category WHERE product.productId=:productId AND stateDb<>:stateDb")
	List<Category> getListCategoriesOfProduct(@Param("productId") Long productId, @Param("stateDb") String stateDb);

	@Query("FROM Category WHERE stateDb<>:stateDb")
	List<Category> findAll(@Param("stateDb") String stateDb);

	@Override
	default List<Category> findAll() {
		return findAll(EnumStatus.DELETE.toString());
	}

	default List<Category> getListCategoriesOfProduct(Long productId) {
		return getListCategoriesOfProduct(productId, EnumStatus.DELETE.toString());
	}

	default Category getReferenceById(Long categoryId) {
		return getReferenceById(categoryId, EnumStatus.DELETE.toString());
	}
}
