package ingeniance.org.kata.supermarket.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ingeniance.org.kata.supermarket.data.schema.Payment;
import ingeniance.org.kata.supermarket.service.PaymentService;

@RestController
@RequestMapping(path = "/kata/supermarket/payment")
public class PaymentController {

	@Autowired
	private PaymentService paymentService;

	@PostMapping(path = "/save", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Payment> save(@RequestBody Payment payment) throws Exception {
		payment = paymentService.save(payment);
		return new ResponseEntity<Payment>(payment, HttpStatus.OK);
	}

	@GetMapping(path = "/history", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<List<Payment>> getPaymentsHistory() throws Exception {
		return new ResponseEntity<List<Payment>>(paymentService.getPaymentsHistory(), HttpStatus.OK);
	}

	@GetMapping(path = "/{payment}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Payment> getReferenceById(@PathVariable("payment") Long paymentId) throws Exception {
		return new ResponseEntity<Payment>(paymentService.getReferenceById(paymentId), HttpStatus.OK);
	}
}
