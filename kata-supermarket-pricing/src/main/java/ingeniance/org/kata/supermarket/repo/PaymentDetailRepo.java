package ingeniance.org.kata.supermarket.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ingeniance.org.kata.supermarket.data.schema.PaymentDetail;

public interface PaymentDetailRepo extends JpaRepository<PaymentDetail, Long> {

	@Query("FROM PaymentDetail WHERE rate.rateId=:rateId")
	List<PaymentDetail> getAllPaymentDetailsOfRate(@Param("rateId") Long rateId);

	@Query("FROM PaymentDetail WHERE payment.paymentId=:paymentId")
	List<PaymentDetail> getDetailsofPayment(@Param("paymentId") Long paymentId);
}
