package ingeniance.org.kata.supermarket.business;

import java.util.HashMap;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ingeniance.org.kata.supermarket.data.schema.PaymentDetail;
import ingeniance.org.kata.supermarket.data.schema.Rate;
import ingeniance.org.kata.supermarket.enums.EnumStatus;
import ingeniance.org.kata.supermarket.service.RateService;

@Service
public class SuperMarketBusiness {

	@Autowired
	private RateService rateService;
	public static final HashMap<String, Double> conversionArray = new HashMap<String, Double>() {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			put(EnumStatus.Pound.toString(), 16D);
			put(EnumStatus.Kilogramm.toString(), 35.274D);
			put(EnumStatus.Gramm.toString(), 0.035274D);
			put(EnumStatus.Ounce.toString(), 1D);
		}
	};

	public double calculPrincing(Set<PaymentDetail> paymentDetailSet) throws Exception {
		double totalAmount = 0d;
		for (PaymentDetail payDetail : paymentDetailSet) {
			Double qte;
			Rate rate = rateService.getReferenceById(payDetail.getRate().getRateId());

			if (payDetail.getUnity() != null && !payDetail.getUnity().equals(rate.getCategory().getUnity())) {
				qte = rate.getCategory().getQuantity() * conversionArray.get(rate.getCategory().getUnity())
						/ conversionArray.get(payDetail.getUnity());

			} else {
				qte = Double.valueOf(rate.getQuantity());
			}

			Double amountRate = rate.getQuantityAmount();
			int qtePay = payDetail.getQuantity();
			Double amountDetail = qtePay * amountRate / qte;
			totalAmount += amountDetail.intValue() + (amountDetail > amountDetail.intValue() ? 1D : 0D);
		}
		return totalAmount;
	}

}
