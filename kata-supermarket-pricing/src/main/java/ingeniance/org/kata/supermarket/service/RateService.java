package ingeniance.org.kata.supermarket.service;

import java.util.List;

import ingeniance.org.kata.supermarket.data.schema.Rate;

public interface RateService {
	Rate save(Rate rate) throws Exception;

	Rate delete(Rate rate) throws Exception;

	Rate doExpireRate(Rate rate) throws Exception;

	List<Rate> getAllRates() throws Exception;

	List<Rate> getRatesOfCategory(Long categoryId) throws Exception;

	List<Rate> getRatesInProgress() throws Exception;

	List<Rate> getHistoryRates(Long categoryId) throws Exception;

	Rate getReferenceById(Long rateId) throws Exception;
}
