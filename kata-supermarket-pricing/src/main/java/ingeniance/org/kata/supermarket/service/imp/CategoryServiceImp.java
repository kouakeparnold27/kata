package ingeniance.org.kata.supermarket.service.imp;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ingeniance.org.kata.supermarket.data.schema.Category;
import ingeniance.org.kata.supermarket.data.schema.Product;
import ingeniance.org.kata.supermarket.data.schema.Rate;
import ingeniance.org.kata.supermarket.enums.EnumStatus;
import ingeniance.org.kata.supermarket.repo.CategoryRepo;
import ingeniance.org.kata.supermarket.service.CategoryService;
import ingeniance.org.kata.supermarket.service.ProductService;
import ingeniance.org.kata.supermarket.service.RateService;

@Service
public class CategoryServiceImp implements CategoryService {

	@Autowired
	private CategoryRepo categoryRepo;

	@Autowired
	private ProductService productService;

	@Autowired
	private RateService rateService;

	/*
	 * Cree une nouvelle categorie d'un produit. Un produit a plusieurs categories
	 * Une categorie doit avoir un produit Une categorie peut etre modifiee
	 */
	public Category save(Category category) throws Exception {
		// Une categorie sans produit doit renvoyer une exception.
		if (category.getProduct() == null)
			throw new Exception("The product of category cannot be null.");

		Product productOpt = productService.getReferenceId(category.getProduct().getProductId());
		if (productOpt == null)
			throw new Exception("The product of category " + category.getProduct().getCode() + " doesn't exist.");
		if (category.getCategoryId() == null) {
			category.setStateDb(EnumStatus.CREATE.toString());
			category.setDtCreated(new Date());
		} else {
			category.setStateDb(EnumStatus.UPDATE.toString());
		}
		category.setProduct(productOpt);
		category.setDtModified(new Date());
		category.setStateDbUniq(0L);
		return categoryRepo.save(category);
	}

	/*
	 * Suppression d'une categorie. Une categorie qui contient un tarif en cours ne
	 * peut pas etre supprime. Il faut d'abord expirer tous les tarifs avant de
	 * supprimer une categorie.
	 */
	public Category delete(Category category) throws Exception {
		category = categoryRepo.getReferenceById(category.getCategoryId());
		List<Rate> rates = rateService.getRatesOfCategory(category.getCategoryId());

		if (!rates.isEmpty()) {
			throw new Exception("The Category content Rates In progress; could not be delete.");
		}
		category.setDtModified(new Date());
		category.setStatus(EnumStatus.EXPIRED.toString());
		category.setStateDb(EnumStatus.DELETE.toString());
		category.setStateDbUniq(category.getCategoryId());
		return categoryRepo.save(category);
	}

	/*
	 * Retourne la reference de la categorie
	 */
	public Category getReferenceId(Long categoryId) throws Exception {
		return categoryRepo.getReferenceById(categoryId);
	}

	/*
	 * Retourne la liste de categories d'un produit
	 */
	public List<Category> getListCategoriesOfProduct(Long productId) throws Exception {
		return categoryRepo.getListCategoriesOfProduct(productId);
	}

	/*
	 * Retourne toutes les categories
	 */
	@Override
	public List<Category> getAllCategories() throws Exception {
		return categoryRepo.findAll();
	}
}
