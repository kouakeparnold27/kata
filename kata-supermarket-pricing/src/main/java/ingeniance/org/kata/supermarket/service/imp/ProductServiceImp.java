package ingeniance.org.kata.supermarket.service.imp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ingeniance.org.kata.supermarket.data.schema.Category;
import ingeniance.org.kata.supermarket.data.schema.Product;
import ingeniance.org.kata.supermarket.enums.EnumStatus;
import ingeniance.org.kata.supermarket.repo.ProductRepo;
import ingeniance.org.kata.supermarket.service.CategoryService;
import ingeniance.org.kata.supermarket.service.ProductService;

@Service
public class ProductServiceImp implements ProductService {

	@Autowired
	private ProductRepo productRepo;

	@Autowired
	private CategoryService categoryService;

	/*
	 * Enregistrement d'un nouveau produit Un produit peut etre modifie.
	 * 
	 */

	public Product save(Product product) throws Exception {
		if (product.getProductId() == null)
			product.setStateDb(EnumStatus.CREATE.toString());
		else {
			Optional<Product> productOld = productRepo.findById(product.getProductId(), EnumStatus.DELETE.toString());
			if (productOld.isPresent() && !productOld.get().getCode().equals(product.getCode())) {
				throw new Exception("The code could not be modified.");
			}
			product.setStateDb(EnumStatus.UPDATE.toString());
		}

		product.setStateDbUniq(0L);
		return productRepo.save(product);
	}

	/*
	 * Suppression d'un produit Un produit qui a au moins une categorie ne peut etre
	 * supprimee. Pour supprimer un produit, il faut d'abord supprimer toutes ses
	 * categories
	 */
	public void delete(Product product) throws Exception {
		product = productRepo.getReferenceById(product.getProductId());
		// Un produit qui est categorise non expire, ne peut etre supprime.
		List<Category> categories = categoryService.getListCategoriesOfProduct(product.getProductId());
		if (!categories.isEmpty())
			throw new Exception("The product content Categories; could not be delete.");

		product.setStateDb(EnumStatus.DELETE.toString());
		product.setStateDbUniq(product.getProductId());
		productRepo.save(product);

	}

	/*
	 * Liste tous les produits.
	 */
	public List<Product> getAllProducts() throws Exception {
		return productRepo.findAll();
	}

	/*
	 * Retourne le produit idtenfie par le code.
	 */
	@Override
	public Product getProductByCode(String code) throws Exception {
		return productRepo.getProductByCode(code);
	}

	@Override
	public Product getReferenceId(Long productId) throws Exception {
		return productRepo.getReferenceById(productId);
	}
}
