package ingeniance.org.kata.supermarket.service;

public interface SuperMarketService {
	
	String getSimplePrice(Long categoryId) throws Exception;

	String getTreeForOneDollar(Long paymemtId) throws Exception;
}
