package ingeniance.org.kata.supermarket.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ingeniance.org.kata.supermarket.data.schema.Payment;

public interface PaymentRepo extends JpaRepository<Payment, Long> {

	@Query("FROM Payment ORDER BY dtPayment DESC")
	List<Payment> getPaymentsHistory();

}
