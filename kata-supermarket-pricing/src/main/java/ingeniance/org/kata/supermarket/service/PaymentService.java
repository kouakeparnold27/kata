package ingeniance.org.kata.supermarket.service;

import java.util.List;

import ingeniance.org.kata.supermarket.data.schema.Payment;

public interface PaymentService {

	Payment save(Payment payment) throws Exception;

	List<Payment> getPaymentsHistory() throws Exception;

	Payment getReferenceById(Long paymentId);
}
