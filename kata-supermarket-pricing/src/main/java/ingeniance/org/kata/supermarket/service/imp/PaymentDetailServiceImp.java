package ingeniance.org.kata.supermarket.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ingeniance.org.kata.supermarket.data.schema.PaymentDetail;
import ingeniance.org.kata.supermarket.data.schema.Rate;
import ingeniance.org.kata.supermarket.enums.EnumStatus;
import ingeniance.org.kata.supermarket.repo.PaymentDetailRepo;
import ingeniance.org.kata.supermarket.service.PaymentDetailService;
import ingeniance.org.kata.supermarket.service.RateService;

@Service
public class PaymentDetailServiceImp implements PaymentDetailService {

	@Autowired
	private PaymentDetailRepo paymentDetailRepo;

	@Autowired
	private RateService rateService;

	/*
	 * Creation d'un detail de paiement un detail de paiement correspond a une
	 * quantie choisi sur un tarif un cours.
	 */
	@Override
	public PaymentDetail save(PaymentDetail paymentDetail) throws Exception {
		if (paymentDetail.getPaymentDetailId() != null)
			throw new Exception("Operation not granted");

		if (paymentDetail.getPayment() == null || paymentDetail.getRate() == null)
			throw new Exception("Error in process payment.");

		Rate rateOld = rateService.getReferenceById(paymentDetail.getRate().getRateId());

		if (rateOld == null) {
			throw new Exception("The rate cannot be null.");
		} else if (rateOld != null && !rateOld.getRateId().equals(paymentDetail.getRate().getRateId())) {
			throw new Exception("The code could not be modified.");
		} else if (rateOld.getState().equals(EnumStatus.EXPIRED.toString())) {
			throw new Exception("The rate is Expired");
		}

		if (paymentDetail.getPayment() == null) {
			throw new Exception("The payment cannot be null.");
		}

		if (paymentDetail.getQuantity() < 0) {
			throw new Exception("Bad quantity value for the payment.");
		}

		return paymentDetailRepo.save(paymentDetail);
	}

	@Override
	public List<PaymentDetail> getAllPaymentDetailsOfRate(Long rateId) throws Exception {
		return paymentDetailRepo.getAllPaymentDetailsOfRate(rateId);
	}

	@Override
	public List<PaymentDetail> getDetailsofPayment(Long paymentId) throws Exception {
		return paymentDetailRepo.getDetailsofPayment(paymentId);
	}

	@Override
	public PaymentDetail getReferenceById(Long paymentDetailId) {
		return paymentDetailRepo.getReferenceById(paymentDetailId);
	}
}
