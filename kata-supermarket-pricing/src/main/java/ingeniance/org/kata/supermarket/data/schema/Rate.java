package ingeniance.org.kata.supermarket.data.schema;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "rate", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "category_id", "quantity", "quantity_amount", "state_db_uniq" }) })
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property = "@rate", scope = Rate.class, resolver = ObjectIdResolver.class)
public class Rate implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "rate_id", nullable = false)
	private Long rateId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "category_id")
	private Category category;

	@Column(nullable = false)
	private int quantity;

	@Column(name = "quantity_amount", nullable = false, precision = 15, scale = 3)
	private double quantityAmount;

	@Column(length = 10)
	private String state;

	@Column(name = "dt_created", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtCreated;

	@Column(name = "dt_modified", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtModified;

	@Column(name = "state_db", nullable = false, length = 10)
	private String stateDb;

	@Column(name = "state_db_uniq", nullable = false)
	private long stateDbUniq;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "rate", fetch = FetchType.LAZY)
	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	@JsonIgnore
	private Set<PaymentDetail> paymentDetailSet;
}
