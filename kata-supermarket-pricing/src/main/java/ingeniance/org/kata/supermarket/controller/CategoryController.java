package ingeniance.org.kata.supermarket.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ingeniance.org.kata.supermarket.data.schema.Category;
import ingeniance.org.kata.supermarket.service.CategoryService;

@RestController
@RequestMapping(path = "/kata/supermarket/category")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;

	@PostMapping(path = "/save", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Category> save(@RequestBody Category category) throws Exception {
		category = categoryService.save(category);
		return new ResponseEntity<Category>(category, HttpStatus.OK);
	}

	@DeleteMapping(path = "/delete", consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Long> delete(@RequestBody Category category) throws Exception {
		categoryService.delete(category);
		return new ResponseEntity<Long>(category.getCategoryId(), HttpStatus.OK);
	}

	@GetMapping(path = "/all", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<List<Category>> getAllCategories() throws Exception {
		return new ResponseEntity<List<Category>>(categoryService.getAllCategories(), HttpStatus.OK);
	}
	
	@GetMapping(path = "/", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
		public ResponseEntity<List<Category>> getCategoriesOfProduct(@RequestParam("product") Long productId) throws Exception {
		return new ResponseEntity<List<Category>>(categoryService.getListCategoriesOfProduct(productId), HttpStatus.OK);
	}
}
