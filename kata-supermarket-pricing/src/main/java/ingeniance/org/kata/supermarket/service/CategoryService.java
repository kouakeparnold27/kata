package ingeniance.org.kata.supermarket.service;

import java.util.List;

import ingeniance.org.kata.supermarket.data.schema.Category;

public interface CategoryService {
	
	Category save(Category category) throws Exception;

	Category delete(Category category) throws Exception;

	public List<Category> getAllCategories() throws Exception;
	
	List<Category> getListCategoriesOfProduct(Long productId) throws Exception;
	
	Category getReferenceId(Long categoryId) throws Exception;
}
