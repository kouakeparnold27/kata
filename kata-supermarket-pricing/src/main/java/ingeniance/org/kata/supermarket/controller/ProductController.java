package ingeniance.org.kata.supermarket.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ingeniance.org.kata.supermarket.data.schema.Product;
import ingeniance.org.kata.supermarket.service.ProductService;

@RestController
@RequestMapping(path = "/kata/supermarket/product")
public class ProductController {

	@Autowired
	private ProductService productService;

	@PostMapping(path = "/save", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Product> save(@RequestBody Product product) throws Exception {
		product = productService.save(product);
		return new ResponseEntity<Product>(product, HttpStatus.OK);
	}

	@DeleteMapping(path = "/delete", consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Long> delete(@RequestBody Product product) throws Exception {
		productService.delete(product);
		return new ResponseEntity<Long>(product.getProductId(), HttpStatus.OK);
	}

	@GetMapping(path = "/all", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<List<Product>> getAllProducts() throws Exception {
		return new ResponseEntity<List<Product>>(productService.getAllProducts(), HttpStatus.OK);
	}

	@GetMapping(path = "/{code}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Product> getProductByCode(@PathVariable("code") String code) throws Exception {
		return new ResponseEntity<Product>(productService.getProductByCode(code), HttpStatus.OK);
	}
}
