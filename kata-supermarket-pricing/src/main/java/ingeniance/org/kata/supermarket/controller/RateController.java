package ingeniance.org.kata.supermarket.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ingeniance.org.kata.supermarket.data.schema.Rate;
import ingeniance.org.kata.supermarket.service.RateService;

@RestController
@RequestMapping(path = "/kata/supermarket/rate")
public class RateController {

	@Autowired
	private RateService rateService;

	@PostMapping(path = "/save", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Rate> save(@RequestBody Rate rate) throws Exception {
		rate = rateService.save(rate);
		return new ResponseEntity<Rate>(rate, HttpStatus.OK);
	}

	@DeleteMapping(path = "/delete", consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Long> delete(@RequestBody Rate rate) throws Exception {
		rateService.delete(rate);
		return new ResponseEntity<Long>(rate.getRateId(), HttpStatus.OK);
	}

	@GetMapping(path = "/all", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<List<Rate>> getAllRates() throws Exception {
		return new ResponseEntity<List<Rate>>(rateService.getAllRates(), HttpStatus.OK);
	}

	@GetMapping(path = "/", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<List<Rate>> getRatesOfCategoriy(@RequestParam("category") Long categoryId) throws Exception {
		return new ResponseEntity<List<Rate>>(rateService.getRatesOfCategory(categoryId), HttpStatus.OK);
	}

	@GetMapping(path = "/in-progress", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<List<Rate>> getRatesInProgress() throws Exception {
		return new ResponseEntity<List<Rate>>(rateService.getRatesInProgress(), HttpStatus.OK);
	}

	@GetMapping(path = "/history", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<List<Rate>> getHistoryRates(@RequestParam("category") Long categoryId) throws Exception {
		return new ResponseEntity<List<Rate>>(rateService.getHistoryRates(categoryId), HttpStatus.OK);
	}
}
