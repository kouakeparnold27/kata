package ingeniance.org.kata.supermarket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ingeniance.org.kata.supermarket.service.SuperMarketService;

@RestController
@RequestMapping(path = "/kata/supermarket")
public class SuperMarketController {

	@Autowired
	private SuperMarketService superMarketService;

	@GetMapping(path = "/price/{category}")
	public ResponseEntity<String> getPrice(@PathVariable Long category) throws Exception {
		return new ResponseEntity<String>(superMarketService.getSimplePrice(category), HttpStatus.OK);
	}

	@GetMapping(path = "/price")
	public ResponseEntity<String> getTreeForOneDollar(@RequestParam("payment") Long paymentId) throws Exception {
		return new ResponseEntity<String>(superMarketService.getTreeForOneDollar(paymentId), HttpStatus.OK);
	}

}
