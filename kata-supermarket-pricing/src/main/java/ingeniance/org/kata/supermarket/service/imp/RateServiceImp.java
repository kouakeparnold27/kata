package ingeniance.org.kata.supermarket.service.imp;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ingeniance.org.kata.supermarket.data.schema.Category;
import ingeniance.org.kata.supermarket.data.schema.Rate;
import ingeniance.org.kata.supermarket.enums.EnumStatus;
import ingeniance.org.kata.supermarket.repo.RateRepo;
import ingeniance.org.kata.supermarket.service.CategoryService;
import ingeniance.org.kata.supermarket.service.RateService;

@Service
public class RateServiceImp implements RateService {

	@Autowired
	private RateRepo rateRepo;

	@Autowired
	private CategoryService categoryService;

	/*
	 * Cree une nouveau tarif a partir d'une categorie d'un produit Par defaut a la
	 * creation le tarif est en cours. Le tarif ne peut pas etre modifie, toutefois,
	 * il peut etre expire, dans ce cas, les informations ne doivent pas changees.
	 */
	public Rate save(Rate rate) throws Exception {
		if (rate.getCategory() == null)
			throw new Exception("The Category of rate cannot be null.");

		Category categoryOpt = categoryService.getReferenceId(rate.getCategory().getCategoryId());
		if (categoryOpt == null)
			throw new Exception("The Category " + rate.getCategory().getCategoryId() + " doesn't exist.");

		rate.setCategory(categoryOpt);

		if (rate.getRateId() == null) {
			rate.setState(EnumStatus.INPROGRESS.toString());
			rate.setStateDb(EnumStatus.CREATE.toString());
			rate.setDtCreated(new Date());
		} else {
			throw new Exception("The rate cannot be modified.");
		}
		rate.setDtModified(new Date());
		rate.setStateDbUniq(0L);

		return rateRepo.save(rate);
	}

	/*
	 * Suppression d'un tarif. La suppression d'un tarif l'expire automatiquement.
	 */
	public Rate delete(Rate rate) throws Exception {
		rate = rateRepo.getReferenceById(rate.getRateId());
		rate.setState(EnumStatus.EXPIRED.toString());
		rate.setStateDb(EnumStatus.DELETE.toString());
		rate.setStateDbUniq(rate.getRateId());
		rate.setDtModified(new Date());

		return rateRepo.save(rate);
	}

	/*
	 * Expiration d'un tarif La suppression d'un tarif l'expire automatiquement.
	 */
	@Override
	public Rate doExpireRate(Rate rate) throws Exception {
		if (rate.getCategory() == null)
			throw new Exception("The product of category cannot be null.");

		Category categoryOpt = categoryService.getReferenceId(rate.getCategory().getCategoryId());
		if (categoryOpt == null)
			throw new Exception("The Rate of category " + rate.getCategory().getCategoryId() + " doesn't exist.");

		rate.setCategory(categoryOpt);

		if (rate.getRateId() == null)
			throw new Exception("The product of category " + rate.getCategory().getCategoryId() + " doesn't exist.");

		Rate rateOld = rateRepo.getReferenceById(rate.getRateId());
		if (!rateOld.getCategory().getCategoryId().equals(rate.getCategory().getCategoryId())) {
			throw new Exception("The rate cannot be modified.");
		}

		rate.setStateDb(EnumStatus.UPDATE.toString());
		rate.setState(EnumStatus.EXPIRED.toString());
		rate.setDtModified(new Date());

		return rateRepo.save(rate);
	}

	public List<Rate> getAllRates() throws Exception {
		return rateRepo.findAll();
	}

	public Rate getReferenceById(Long rateId) throws Exception {
		return rateRepo.getReferenceById(rateId);
	}

	public List<Rate> getRatesOfCategory(Long categoryId) throws Exception {
		return rateRepo.getAllRatesOfCategory(categoryId);
	}

	@Override
	public List<Rate> getRatesInProgress() throws Exception {
		return rateRepo.getRatesByState(EnumStatus.INPROGRESS.toString());
	}

	@Override
	public List<Rate> getHistoryRates(Long categoryId) throws Exception {
		return rateRepo.getHistoryRatesByCategory(categoryId);
	}
}
