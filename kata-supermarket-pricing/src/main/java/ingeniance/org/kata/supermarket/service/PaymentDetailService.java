package ingeniance.org.kata.supermarket.service;

import java.util.List;

import ingeniance.org.kata.supermarket.data.schema.PaymentDetail;

public interface PaymentDetailService {
	
	PaymentDetail save(PaymentDetail paymentDetail) throws Exception;

	List<PaymentDetail> getAllPaymentDetailsOfRate(Long rateId) throws Exception;

	List<PaymentDetail> getDetailsofPayment(Long paymentId) throws Exception;

	PaymentDetail getReferenceById(Long paymentDetailId);
}
