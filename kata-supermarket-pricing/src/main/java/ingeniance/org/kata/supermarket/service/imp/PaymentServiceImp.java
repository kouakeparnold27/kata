package ingeniance.org.kata.supermarket.service.imp;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ingeniance.org.kata.supermarket.business.SuperMarketBusiness;
import ingeniance.org.kata.supermarket.data.schema.Payment;
import ingeniance.org.kata.supermarket.repo.PaymentRepo;
import ingeniance.org.kata.supermarket.service.PaymentService;

@Service
public class PaymentServiceImp implements PaymentService {

	@Autowired
	private PaymentRepo paymentRepo;

	@Autowired
	private SuperMarketBusiness spmBusiness;

	/*
	 * Creation du paeiment des produits. le paiement regroupe la liste des details
	 * de paiements qui sont effectues sur les differents tarifs en cours.
	 */
	public Payment save(Payment payment) throws Exception {
		if (payment.getPaymentId() == null) {
			payment.setDtPayment(new Date());
		} else {
			throw new Exception("Operation not granted");
		}

		if (payment.getPaymentDetailSet() == null || payment.getPaymentDetailSet().size() == 0) {
			throw new Exception("This payment has no detail.");
		}

		double totalAmount = spmBusiness.calculPrincing(payment.getPaymentDetailSet());

		payment.setTotalAmount(totalAmount);

		return paymentRepo.save(payment);
	}

	/*
	 * Historique de paiement
	 */
	@Override
	public List<Payment> getPaymentsHistory() throws Exception {
		return paymentRepo.getPaymentsHistory();
	}

	/*
	 * Reference d'un paiement et ses details de paiement
	 */
	@Override
	public Payment getReferenceById(Long paymentId) {
		return paymentRepo.getReferenceById(paymentId);
	}
}
