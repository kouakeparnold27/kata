Bonjour.

Suite à une erreur d'authentification à clé public sur GitHub,

j'ai dû reprendre l'historique en le publiant sur GitLab.

Le projet GitLab n'ayant plus les mêmes dates d'historique,

une capture d'ecran a été jointe au projet pour determiner

les dates originales de chaque commit.

Le meilleur reste a venir;

cordialement.