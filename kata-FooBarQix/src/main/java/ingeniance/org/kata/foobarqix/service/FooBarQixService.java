package ingeniance.org.kata.foobarqix.service;

public interface FooBarQixService {

	String printFooBarQix(int number) throws Exception;

}
