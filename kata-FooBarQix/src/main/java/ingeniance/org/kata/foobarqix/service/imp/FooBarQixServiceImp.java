package ingeniance.org.kata.foobarqix.service.imp;

import org.springframework.stereotype.Component;

import ingeniance.org.kata.foobarqix.service.FooBarQixService;

@Component
public class FooBarQixServiceImp implements FooBarQixService {

	private static final String[] values = { "Foo", "Bar", "Qix" };

	/**
	 * If the number is divisible by 3 or contains 3, replace 3 by "Foo";
	 * if the number is divisible by 5 or contains 5, replace 5 by "Bar";
	 * if the number is divisible by 7 or contains 7, replace 7 by "Qix".
	 * 
	 * @param number
	 * @return result
	 * @throws Exception 
	 * 
	 */
	public String printFooBarQix(int number) throws Exception {
		if (number < 0)
			throw new Exception("The number input could not be negative.");
		
		String result = "";
		if (number % 3 == 0)
			result += values[0];
		if (number % 5 == 0)
			result += values[1];
		if (number % 7 == 0)
			result += values[2];

		String numberStr = String.valueOf(number);

		for (int i = 0; i < numberStr.length(); i++) {
			switch (numberStr.charAt(i)) {
			case '3':
				result += values[0];
				break;
			case '5':
				result += values[1];
				break;
			case '7':
				result += values[2];
				break;
			default:
				break;
			}
		}

		return result;
	}
}
