package ingeniance.org.kata.foobarqix.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ingeniance.org.kata.foobarqix.service.FooBarQixService;

@RestController
@RequestMapping(path = "/kata/fbq")
public class FooBarQixController {
	
	@Autowired
	FooBarQixService fooBarQixService;
	
	@GetMapping(path = "/{number}")
	public ResponseEntity<String> printDiamond(@PathVariable("number") int number) {
		String fooBarQixPrinted = null;
		try {
			fooBarQixPrinted = fooBarQixService.printFooBarQix(number);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<String>(fooBarQixPrinted, HttpStatus.OK);
	}
}
