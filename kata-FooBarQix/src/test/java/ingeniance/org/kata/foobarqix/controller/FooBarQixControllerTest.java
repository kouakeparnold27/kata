package ingeniance.org.kata.foobarqix.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

@SpringBootTest
@AutoConfigureMockMvc
public class FooBarQixControllerTest {
	@Autowired
	private MockMvc mvc;

	@Test
	public void printFooBarQixTestWithBadNumber() throws Exception {
		mvc.perform(get("/kata/fbq/{number}", "-").accept(MediaType.TEXT_PLAIN)).andExpect(status().isBadRequest());
	}

	@Test
	public void printFooBarQixTestWithEmptyLetter() throws Exception {
		mvc.perform(get("/kata/fbq/{number}", "").accept(MediaType.TEXT_PLAIN)).andExpect(status().isNotFound());
	}

	@Test
	public void printFooBarQixTestNegative() throws Exception {
		mvc.perform(get("/kata/fbq/{number}", -1).accept(MediaType.TEXT_PLAIN)).andExpect(status().isBadRequest());
	}

	@Test
	public void printFooBarQixTest51() throws Exception {
		mvc.perform(get("/kata/fbq/{number}", 51).accept(MediaType.TEXT_PLAIN)).andExpect(status().isOk())
				.andExpect(content().string("FooBar")).andReturn();
	}

	@Test
	public void printFooBarQixTest53() throws Exception {
		mvc.perform(get("/kata/fbq/{number}", 53).accept(MediaType.TEXT_PLAIN)).andExpect(status().isOk())
				.andExpect(content().string("BarFoo")).andReturn();
	}

	@Test
	public void printFooBarQixTest13() throws Exception {
		mvc.perform(get("/kata/fbq/{number}", 13).accept(MediaType.TEXT_PLAIN)).andExpect(status().isOk())
				.andExpect(content().string("Foo")).andReturn();
	}

	@Test
	public void printFooBarQixTes15() throws Exception {
		mvc.perform(get("/kata/fbq/{number}", 15).accept(MediaType.TEXT_PLAIN)).andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(content().string("FooBarBar")).andReturn();
	}

	@Test
	public void printFooBarQixTest33() throws Exception {
		mvc.perform(get("/kata/fbq/{number}", 33).accept(MediaType.TEXT_PLAIN)).andExpect(status().isOk())
				.andExpect(content().string("FooFooFoo")).andReturn();
		;

	}

}
