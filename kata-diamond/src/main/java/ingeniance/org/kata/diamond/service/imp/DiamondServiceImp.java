package ingeniance.org.kata.diamond.service.imp;

import java.util.Collections;

import org.springframework.stereotype.Component;

import ingeniance.org.kata.diamond.service.DiamondService;

@Component
public class DiamondServiceImp implements DiamondService {

	public static final String ALPHABET = "abcdefghijklmnopqrstuvwxyz";
	
	
	/**
	 * This function print diamond from a letter
	 * @param letter
	 * @return strDiamond
	 * @throws Exception
	 * 
	 * */
	public String printDiamond(CharSequence letter) throws Exception {
		boolean isUpperCase = false;
		if(letter.length()==0 || letter.length()>1)
			throw new Exception("You must write one letter.");
			
		isUpperCase = Character.isUpperCase(letter.charAt(0));
		if(!(ALPHABET.contains(letter) || (isUpperCase && ALPHABET.toUpperCase().contains(letter)))) {
			throw new Exception("The letter input is not in a list of alphabet.");
		}
		int letterPosition = isUpperCase ? ALPHABET.toUpperCase().indexOf(letter.toString()) : ALPHABET.indexOf(letter.toString());
		String strDiamond = "";
		
		for(int i=0; i < letterPosition+1; i++) {
			strDiamond += String.join("", Collections.nCopies((letterPosition - i), "  ")) + (isUpperCase ? ALPHABET.toUpperCase().charAt(i) : ALPHABET.charAt(i))+
					(i>0 ? String.join("", Collections.nCopies(i*2, "  ")) + (isUpperCase ? ALPHABET.toUpperCase().charAt(i) : ALPHABET.charAt(i)) : "")
					+"\n";
		}

		for(int i=letterPosition-1; i >= 0; i--) {
			strDiamond += String.join("", Collections.nCopies(letterPosition-i, "  ")) + (isUpperCase ? ALPHABET.toUpperCase().charAt(i) : ALPHABET.charAt(i))
					+ (i>0 ? String.join("", Collections.nCopies((i)*2, "  ")) + (isUpperCase ? ALPHABET.toUpperCase().charAt(i) : ALPHABET.charAt(i)) : "")
					+"\n";
		}
		return strDiamond;
	}
}
