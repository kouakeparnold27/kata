package ingeniance.org.kata.diamond.service;

public interface DiamondService {

	public String printDiamond(CharSequence letter) throws Exception;
}
