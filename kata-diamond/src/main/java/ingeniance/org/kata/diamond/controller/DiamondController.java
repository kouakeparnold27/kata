package ingeniance.org.kata.diamond.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ingeniance.org.kata.diamond.service.DiamondService;

@RestController
@RequestMapping(path = "/kata/diamond")
public class DiamondController {
	
	@Autowired
	DiamondService diamondService;
	
	@GetMapping(path = "/{letter}")
	public ResponseEntity<String> printDiamond(@PathVariable("letter") CharSequence letter) {
		String diamondPrinted = null;
		try {
			diamondPrinted = diamondService.printDiamond(letter);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<String>(diamondPrinted, HttpStatus.OK);
	}
}
