package ingeniance.org.kata.diamond.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

@SpringBootTest
@AutoConfigureMockMvc
public class DiamondControllerTest {
	@Autowired
	private MockMvc mvc;

	@Test
	public void printDiamondTestWithBadLetter() throws Exception {
		mvc.perform(get("/kata/diamond/{letter}", "-").accept(MediaType.TEXT_PLAIN)).andExpect(status().isBadRequest());
	}

	@Test
	public void printDiamondTestWithEmptyLetter() throws Exception {
		mvc.perform(get("/kata/diamond/{letter}", "").accept(MediaType.TEXT_PLAIN)).andExpect(status().isNotFound());
	}

	@Test
	public void printDiamondTesta() throws Exception {
		mvc.perform(get("/kata/diamond/{letter}", "a").accept(MediaType.TEXT_PLAIN)).andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk())
//				.andExpect(content().string("a"))
				.andReturn();
	}
	
	@Test
	public void printDiamondTestb() throws Exception {
		mvc.perform(get("/kata/diamond/{letter}", "b").accept(MediaType.TEXT_PLAIN)).andExpect(status().isOk());

	}
	
	@Test
	public void printDiamondTestC() throws Exception {
		mvc.perform(get("/kata/diamond/{letter}", "C").accept(MediaType.TEXT_PLAIN)).andExpect(status().isOk());
	}
}
