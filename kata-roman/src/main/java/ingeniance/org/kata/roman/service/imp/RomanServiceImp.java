package ingeniance.org.kata.roman.service.imp;

import java.util.Collections;

import org.springframework.stereotype.Component;

import ingeniance.org.kata.roman.service.RomanService;

@Component
public class RomanServiceImp implements RomanService {

	private static final int[] NUMERICAL_VALUES = { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };

	private static final String[] ROMAN_VALUES = { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV",
			"I" };

	/**
	 * Convert number to roman letter
	 * 
	 * @param number
	 * @return romanValue
	 */
	public String convertToRoman(int number) throws Exception {
		if (number < 0)
			throw new Exception("The number input could not be negative.");

		String romanValue = "";
		for (int i = 0; i < NUMERICAL_VALUES.length; i++) {
			int nValue = number / NUMERICAL_VALUES[i];
			romanValue += String.join("", Collections.nCopies(nValue, ROMAN_VALUES[i]));
			number = number % NUMERICAL_VALUES[i];
		}
		return romanValue;
	}
}
