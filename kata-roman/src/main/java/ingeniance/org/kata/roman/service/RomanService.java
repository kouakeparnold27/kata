package ingeniance.org.kata.roman.service;

public interface RomanService {

	String convertToRoman(int number) throws Exception;

}
