package ingeniance.org.kata.roman.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ingeniance.org.kata.roman.service.RomanService;

@RestController
@RequestMapping(path = "/kata/roman")
public class RomanController {

	@Autowired
	private RomanService romanService;

	@GetMapping(path = "/{number}")
	public ResponseEntity<String> printDiamond(@PathVariable("number") int number) {
		String romanResult = null;
		try {
			romanResult = romanService.convertToRoman(number);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(romanResult, HttpStatus.OK);
	}
}
