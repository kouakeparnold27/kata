package ingeniance.org.kata.roman.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

@SpringBootTest
@AutoConfigureMockMvc
public class RomanControllerTest {
	@Autowired
	private MockMvc mvc;

	@Test
	public void printDiamondTestWithBadLetter() throws Exception {
		mvc.perform(get("/kata/roman/{number}", "-").accept(MediaType.TEXT_PLAIN)).andExpect(status().isBadRequest());
	}

	@Test
	public void printDiamondTestWithEmptyLetter() throws Exception {
		mvc.perform(get("/kata/roman/{number}", "").accept(MediaType.TEXT_PLAIN)).andExpect(status().isNotFound());
	}

	@Test
	public void printDiamondTestNegative() throws Exception {
		mvc.perform(get("/kata/roman/{number}", -1).accept(MediaType.TEXT_PLAIN)).andExpect(status().isBadRequest())
				.andReturn();
	}

	@Test
	public void printDiamondTest1() throws Exception {
		mvc.perform(get("/kata/roman/{number}", 1).accept(MediaType.TEXT_PLAIN)).andExpect(status().isOk())
				.andExpect(content().string("I")).andReturn();
	}

	@Test
	public void printDiamondTest4() throws Exception {
		mvc.perform(get("/kata/roman/{number}", 4).accept(MediaType.TEXT_PLAIN)).andExpect(status().isOk())
				.andExpect(content().string("IV")).andReturn();
	}

	@Test
	public void printDiamondTesta() throws Exception {
		mvc.perform(get("/kata/roman/{number}", 1954).accept(MediaType.TEXT_PLAIN)).andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk()).andExpect(content().string("MCMLIV")).andReturn();
	}

	@Test
	public void printDiamondTest1990() throws Exception {
		mvc.perform(get("/kata/roman/{number}", 1990).accept(MediaType.TEXT_PLAIN)).andExpect(status().isOk())
				.andExpect(content().string("MCMXC")).andReturn();
		;

	}

}
